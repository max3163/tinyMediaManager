package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ImdbVideoContentType extends BaseJsonEntity {
  public ImdbLocalizedString displayName = null;
  public String              id          = "";
}
