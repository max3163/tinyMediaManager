package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

/**
 * keywords page
 */
public class ImdbTitleKeyword extends BaseJsonEntity {
  public String id       = "";
  public String rowTitle = "";
}
