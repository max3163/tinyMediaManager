package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ImdbNameText extends BaseJsonEntity {
  public String text = "";
}
