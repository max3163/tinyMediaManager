package org.tinymediamanager.scraper.tvmaze.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class Country extends BaseJsonEntity {
  public String name;
  public String code;
  public String timezone;
}
