package org.tinymediamanager.scraper.tvmaze.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ExternalIds extends BaseJsonEntity {
  public int    tvrage;
  public int    thetvdb;
  public String imdb;
}
