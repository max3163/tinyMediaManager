package org.tinymediamanager.scraper.mdblist.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class MdbIdName extends BaseJsonEntity {
  public int    id;
  public String name;
}
