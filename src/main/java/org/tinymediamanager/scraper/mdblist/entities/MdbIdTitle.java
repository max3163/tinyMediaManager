package org.tinymediamanager.scraper.mdblist.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class MdbIdTitle extends BaseJsonEntity {
  public int    id;
  public String title;
}
